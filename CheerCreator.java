import java.util.Scanner;

public class CheerCreator {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int yard = in.nextInt();

        if (yard > 10) {
            System.out.print("High Five");
        } else if (yard < 1) {
            System.out.print("shh");
        } else {
            for (int i = 0; i < yard; ++i)
                System.out.print("Ra!");
        }
    }
}
