import java.util.Scanner;
import java.lang.Math;

public class Hal_candy {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int houses = in.nextInt();

        int percent = (int) Math.ceil(200 / houses);

        System.out.format("%d", percent);
    }
}
