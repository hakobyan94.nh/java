import  java.util.Scanner;

public class PascalTriangle {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter the height of triangle: ");
        int height = in.nextInt();
        int arr[][] = new int[height][];

        for (int i = 0, j = 1; i < height; ++i) {
            arr[i] = new int[i + 1];
            arr[i][0] = 1;
            arr[i][i] = 1;
            for (; j < i; ++j) {
                arr[i][j] = arr[i - 1][j - 1] + arr[i - 1][j];
            }
            j = 1;
        }

        for (int i = 0; i < height && i < arr[i].length; ++i) {
            for (int j = 0; j < i + 1; ++j) {
                System.out.print(arr[i][j]);
            }
            System.out.println();
        }
    }
}
