import java.util.Scanner;

public class pop {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int siblings, popsicles;

        siblings = in.nextInt();
        popsicles = in.nextInt();

        if (popsicles % siblings == 0) {
            System.out.println("give away");
            return;
        }

        System.out.println("eat them yourself");
    }
}
