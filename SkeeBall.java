import java.util.Scanner;

public class SkeeBall {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int points = in.nextInt();
        int price = in.nextInt();

        if ((points / 12) >= price)
            System.out.println("Buy it!");
        else
            System.out.println("Try again");
    }
}
